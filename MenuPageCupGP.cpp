    struct GameSequenceSection;

    struct GameSequenceSectionVtable { // NOTE: Return types are not correct
        void (*getDTIClassInfo)(GameSequenceSection* own);
        void (*getDTIClass)(GameSequenceSection* own);
        u32 null0;
        void (*_deallocating)(GameSequenceSection* own);
        void (*create)(GameSequenceSection* own, const void* arg);
        void (*init)(GameSequenceSection* own);
        void (*calc)(GameSequenceSection* own);
        void (*render)(GameSequenceSection* own);
        void (*renderMainL)(GameSequenceSection* own);
        void (*renderMainR)(GameSequenceSection* own);
        void (*renderSub)(GameSequenceSection* own);
        u32 null1;
        void (*accept)(GameSequenceSection* own, void* visitor);
        void (*callbackInvokeEventID)(GameSequenceSection* own, int event);
        u32 null2;
        void (*createOuter)(GameSequenceSection* own, const void* unk);
        void (*initOuter)(GameSequenceSection* own);
        u32 null3;
        void (*destroy)(GameSequenceSection* own);
        void (*getSectionType)(GameSequenceSection* own);
        void (*isCompletable)(GameSequenceSection* own);
        void (*isSyncFadein)(GameSequenceSection* own);
        void (*getFadeDelay)(GameSequenceSection* own);
        void (*updateState)(GameSequenceSection* own);
        void (*step)(GameSequenceSection* own);
        void (*ready)(GameSequenceSection* own);
        void (*enter)(GameSequenceSection* own, u32& fadeKind, u32 unkwnown);
        void (*standby)(GameSequenceSection* own);
        void (*start)(GameSequenceSection* own);
        void (*complete)(GameSequenceSection* own);
        void (*cancel)(GameSequenceSection* own, u32& fadeKind, u32 unknown);
        void (*finish)(GameSequenceSection* own, u32& fadeKind, u32 unknown);
        void (*reenter)(GameSequenceSection* own);
        void (*exit)(GameSequenceSection* own);
        void (*clear)(GameSequenceSection* own);
        void (*sceneStart)(GameSequenceSection* own, int code);
        void (*sceneFinish)(GameSequenceSection* own, int code);
        void (*onDestroy)(GameSequenceSection* own);
        void (*onReady)(GameSequenceSection* own);
        void (*onClear)(GameSequenceSection* own);
        void (*pageStep)(GameSequenceSection* own, bool unknown);
        void (*onCreate)(GameSequenceSection* own);
        void (*onGenerateControl)(GameSequenceSection* own, void* controlInitializer);
        void (*onPagePreStep)(GameSequenceSection* own);
        void (*onPagePostStep)(GameSequenceSection* own);
        void (*onPageEnter)(GameSequenceSection* own);
        void (*onPageStandby)(GameSequenceSection* own);
        void (*onPageStart)(GameSequenceSection* own);
        void (*onPageComplete)(GameSequenceSection* own);
        void (*onPageFinish)(GameSequenceSection* own);
        void (*onPageCancel)(GameSequenceSection* own);
        void (*onPageReenter)(GameSequenceSection* own);
        void (*onPageExit)(GameSequenceSection* own);
        void (*canFinishFadein)(GameSequenceSection* own);
        void (*canFinishFadeout)(GameSequenceSection* own);
        void (*completePage)(GameSequenceSection* own, int code);
        void (*getFadeinType)(GameSequenceSection* own, int unknown);
        void (*getFadeoutType)(GameSequenceSection* own, int unknown);
        void (*initControl)(GameSequenceSection* own);
        void (*calcItemIcon)(GameSequenceSection* own);
        void (*onPageFadein)(GameSequenceSection* own);
        void (*onPageFadeout)(GameSequenceSection* own);
        void (*enterCursor)(GameSequenceSection* own, int cursor);
        void (*buttonHandler_SelectOn)(GameSequenceSection* own, int id);
        void (*buttonHandler_OK)(GameSequenceSection* own, int id);
        void (*inputHandler)(GameSequenceSection* own, int unk1, int unk2);
        void (*procOpenMenu)(GameSequenceSection* own);
        void (*procCloseMenu)(GameSequenceSection* own);
        void (*procExitMenu)(GameSequenceSection* own);
        void (*onMenuEnter)(GameSequenceSection* own);
        void (*onMenuStart)(GameSequenceSection* own);
        void (*onMenuComplete)(GameSequenceSection* own);
        void (*onMenuExit)(GameSequenceSection* own);
        void (*onSliderSetting)(GameSequenceSection* own, bool unk1, bool unk2);
        void (*canStartSlideIn)(GameSequenceSection* own);
        void (*canStartSlideOut)(GameSequenceSection* own);
        void (*onTimeUp)(GameSequenceSection* own, int unknown);
        void (*onTimeUpComplete)(GameSequenceSection* own, int unknown);
        void (*onTimeUpCompleteStep)(GameSequenceSection* own, int unknown);
        void (*getBackEnterCode)(GameSequenceSection* own);
        void (*getBackReturnCode)(GameSequenceSection* own);
        void* userData;
        u32 null5;
    };

    struct GameSequenceSection {
        GameSequenceSectionVtable* vtable;

        SeadArrayPtr<VisualControl::GameVisualControl*>& GetButtonArray(u32 offset) {
            return *(SeadArrayPtr<VisualControl::GameVisualControl*>*)((u32)this + offset);
        }

        u32* GetControlInfo() {
            return ((u32***)this)[0x88/4][0];
        }

        u32 GetLastSelectedButton() {
            return GetControlInfo()[0x100/4];
        }

        CursorMove* GetControlMove() {
            return (CursorMove*)(GetControlInfo() + 0xDC/4);
        } 
    };

    class MenuSingleCupBasePage {
        protected:
            VisualControl::GameVisualControl* buttonList[8];
            GameSequenceSection* MenuSingleCupBasePageCons(GameSequenceSection* own);
    };

    class MenuSingleCupGPPage : public MenuSingleCupBasePage{
        public:
            
            MenuSingleCupGPPage() {}

            GameSequenceSection* gameSection;
            GameSequenceSectionVtable vtable;

            GameSequenceSection* Load(void* sectionDirector);

            void Deallocate() {deallocatingBackup(gameSection);}

        private:
            GameSequenceSection* MenuSingleCupGPPageCons(GameSequenceSection* own);
            static void InitControl(GameSequenceSection* own);
            static void OnPageEnter(GameSequenceSection* own);
            static void EnterCursor(GameSequenceSection* own, int cursor);
            static void OnSliderSetting(GameSequenceSection* own, bool flag1, bool flag2);
            static void OnPagePreStep(GameSequenceSection* own);
            static void ButtonHandler_SelectOn(GameSequenceSection* own, int buttonID);
            static void ButtonHandler_OK(GameSequenceSection* own, int buttonID);
            static void OnPageComplete(GameSequenceSection* own);
            static void OnPageExit(GameSequenceSection* own);
            void (*deallocatingBackup)(GameSequenceSection*);
    };

    MenuPageHandler::GameSequenceSection* MenuPageHandler::MenuSingleCupBasePage::MenuSingleCupBasePageCons(GameSequenceSection* own) {
        void*(*MenuMoviePageBaseCons)(GameSequenceSection* own) = (decltype(MenuMoviePageBaseCons))0x004A44CC;
        MenuMoviePageBaseCons(own);
        own->GetButtonArray(0x2E0).SetBuffer(sizeof(buttonList) / sizeof(VisualControl::GameVisualControl*), buttonList);
        ((u32*)own)[0x290/4] = 6;
        return own;
    }

    MenuPageHandler::GameSequenceSection* MenuPageHandler::MenuSingleCupGPPage::MenuSingleCupGPPageCons(GameSequenceSection* own) {
        own = MenuSingleCupBasePage::MenuSingleCupBasePageCons(own);
        memcpy(&vtable, (GameSequenceSectionVtable*)0x00647F5C, sizeof(vtable));
        ((u32*)own)[0] = (u32)&vtable;
        ((u32*)own)[0x314/4] = -1;
        ((u8*)own)[0x318/1] = 0;
        void(*SequenceBasePageInitMenu)(GameSequenceSection*, int) = (decltype(SequenceBasePageInitMenu))0x004D4428;
        SequenceBasePageInitMenu(own, 1);
        ((u8*)own)[0x2BC/1] = 0;

        deallocatingBackup = vtable._deallocating;
        vtable._deallocating = OnMenuSingleCupGPDeallocate;
        vtable.initControl = InitControl;
        vtable.onPageEnter = OnPageEnter;
        vtable.enterCursor = EnterCursor;
        vtable.onSliderSetting = OnSliderSetting;
        vtable.onPagePreStep = OnPagePreStep;
        vtable.buttonHandler_SelectOn = ButtonHandler_SelectOn;
        vtable.buttonHandler_OK = ButtonHandler_OK;
        vtable.onPageComplete = OnPageComplete;
        vtable.onPageExit = OnPageExit;
        vtable.userData = this;
        gameSection = own;
        return own;
    }

    MenuPageHandler::GameSequenceSection* MenuPageHandler::MenuSingleCupGPPage::Load(void* sectionDirector) {
        GameSequenceSection* section = (GameSequenceSection*)ExtraResource::GameAlloc::game_operator_new_autoheap(0x31C);
        section = MenuSingleCupGPPageCons(section);
        ((u32*)section)[0x4/4] = (u32)sectionDirector;
        u32* someArrCount = ((u32*)sectionDirector) + 0x24/4;
        u32* someArrData = *(((u32**)sectionDirector) + 0x1C/4);
        if (someArrData[*someArrCount] == 0) {
            someArrData[*someArrCount] = (u32)section;
            (*someArrCount)++;
        }
        return section;
    }

    void MenuPageHandler::MenuSingleCupGPPage::InitControl(GameSequenceSection* own) {
        u32* ownU32 = (u32*)own;
        VisualControl::GameVisualControl*(*setupOmakaseView)(GameSequenceSection*, const SafeStringBase&, const SafeStringBase&) = 
            (VisualControl::GameVisualControl*(*)(GameSequenceSection*, const SafeStringBase&, const SafeStringBase&))0x005DB094;
        VisualControl::GameVisualControl*(*setupCourseName)(GameSequenceSection*, const SafeStringBase&, const SafeStringBase&) = 
            (VisualControl::GameVisualControl*(*)(GameSequenceSection*, const SafeStringBase&, const SafeStringBase&))0x005DB3C0;
        VisualControl::GameVisualControl*(*setupCourseButtonDummy)(GameSequenceSection*, const SafeStringBase&, const SafeStringBase&) = 
            (VisualControl::GameVisualControl*(*)(GameSequenceSection*, const SafeStringBase&, const SafeStringBase&))0x005DB518;
        VisualControl::GameVisualControl*(*setupRaceDialogButton)(GameSequenceSection*, const SafeStringBase&, const SafeStringBase&, int) = 
            (VisualControl::GameVisualControl*(*)(GameSequenceSection*, const SafeStringBase&, const SafeStringBase&, int))0x005DC9B8;
        VisualControl::GameVisualControl*(*setupBackButton)(GameSequenceSection*, const SafeStringBase&, const SafeStringBase&) = 
            (VisualControl::GameVisualControl*(*)(GameSequenceSection*, const SafeStringBase&, const SafeStringBase&))GameFuncs::SetupControlBackButton;
        VisualControl::GameVisualControl*(*setupBackButtonBothControl)(GameSequenceSection*, const SafeStringBase&, const SafeStringBase&) = 
            (VisualControl::GameVisualControl*(*)(GameSequenceSection*, const SafeStringBase&, const SafeStringBase&))0x005DC080;
        void(*baseMenuButtonControlSetBg)(VisualControl::GameVisualControl*, VisualControl::GameVisualControl*, const SafeStringBase&) =
            (void(*)(VisualControl::GameVisualControl*, VisualControl::GameVisualControl*, const SafeStringBase&))0x00172E18;
        void(*baseMenuButtonControlSetCursor)(VisualControl::GameVisualControl*, VisualControl::GameVisualControl*, const SafeStringBase&) =
            (void(*)(VisualControl::GameVisualControl*, VisualControl::GameVisualControl*, const SafeStringBase&))0x00173254;
        void(*cursorMoveSetType)(CursorMove*, int) =
            (void(*)(CursorMove*, int))0x00143360;
        

        ((void(*)(GameSequenceSection*, int))GameFuncs::BaseMenuPageInitSlider)(own, 3);
        SeadArrayPtr<void*>& controlSliderArray = *(SeadArrayPtr<void*>*)(ownU32 + 0x26C/4);
        u8* defaultSlider = (u8*)controlSliderArray[0];
        defaultSlider[0x1864] = 61;
        defaultSlider[0x1865] = 62;
        
        VisualControl::GameVisualControl* movieView = ((VisualControl::GameVisualControl*(*)(GameSequenceSection*, const SafeStringBase&, const SafeStringBase&, u32))GameFuncs::SetupControlMovieView)
                    (own, SafeStringBase("course_movie"), SafeStringBase("course_movie_T"), 8);
        ownU32[0x2C0/4] = (u32)movieView;
        ((void(*)(VisualControl::GameVisualControl*, const SafeStringBase&))GameFuncs::MovieViewSetMoviePane)(movieView, SafeStringBase("P_movie_dammy"));

        VisualControl::GameVisualControl* omakaseView = setupOmakaseView(own, SafeStringBase("omakase"), SafeStringBase("omakase_T"));
        ownU32[0x2DC/4] = (u32)omakaseView;
        void* movieRootPane = movieView->vtable->getRootPane(movieView);
        ((u32*)omakaseView)[0xA8/4] = (u32)movieRootPane;
        void* omakaseViewRootPane = omakaseView->vtable->getRootPane(omakaseView);
        Vector3* movieRootPanePos = (Vector3*)((u32)movieRootPane + 0x28);
        Vector3* omakaseViewRootPanePos = (Vector3*)((u32)omakaseViewRootPane + 0x28);
        ((float*)omakaseView)[0xAC/4] = omakaseViewRootPanePos->y - movieRootPanePos->y;

        VisualControl::GameVisualControl* courseName = setupCourseName(own, SafeStringBase("course_name"), SafeStringBase("course_name"));
        ownU32[0x2C4/4] = (u32)courseName;

        ((void(*)(void*, VisualControl::GameVisualControl*))GameFuncs::ControlSliderSetSlideH)(controlSliderArray[2], courseName);
        ((void(*)(void*, VisualControl::GameVisualControl*))GameFuncs::ControlSliderSetSlideH)(controlSliderArray[0], movieView);

        VisualControl::GameVisualControl** courseButtonDummies = (VisualControl::GameVisualControl**)(ownU32 + 0x2C8/4);
        courseButtonDummies[0] = setupCourseButtonDummy(own, SafeStringBase("course_btn_dmy"), SafeStringBase("course_btn_dmy_T_0"));
        courseButtonDummies[1] = setupCourseButtonDummy(own, SafeStringBase("course_btn_dmy"), SafeStringBase("course_btn_dmy_T_1"));
        courseButtonDummies[2] = setupCourseButtonDummy(own, SafeStringBase("course_btn_dmy"), SafeStringBase("course_btn_dmy_T_2"));
        courseButtonDummies[3] = setupCourseButtonDummy(own, SafeStringBase("course_btn_dmy"), SafeStringBase("course_btn_dmy_T_3"));
        for (int i = 0; i < 4; i++)
            ((void(*)(void*, VisualControl::GameVisualControl*))GameFuncs::ControlSliderSetSlideH)(controlSliderArray[0], courseButtonDummies[i]);

        bool* isCupLockedArray = (bool*)(ownU32 + 0x30C/4);
        for (int i = 0; i < 8; i++) {
            /* Check if cup is open
            bool isOpen = ((bool(*)(const int*))0x4D158C)(&i);
            */
            bool isOpen = true;
            isCupLockedArray[i] = !isOpen;
        }

        VisualControl::GameVisualControl* cupSelectBg = VisualControl::Build(ownU32, "cup_select_bg", "cup_select_bg", (VisualControl::AnimationDefineVtable*)0x619538, (VisualControl::GameVisualControlVtable*)0x61955C, VisualControl::ControlType::CUP_SELECT_BG_CONTROL);
        ((void(*)(void*, VisualControl::GameVisualControl*))GameFuncs::ControlSliderSetSlideH)(controlSliderArray[1], cupSelectBg);
        ((void(*)(void*, int))0x00481A6C)(controlSliderArray[1], 0); //SetDelayH

        for (int i = 0; i < 8; i++) {
            VisualControl::GameVisualControl* currCupButton = VisualControl::Build(ownU32, "cup_btn", "cup_btn", 0, (VisualControl::GameVisualControlVtable*)0x61EAB4, VisualControl::ControlType::CUP_BTN_CONTROL);
            baseMenuButtonControlSetBg(currCupButton, cupSelectBg, SafeStringBase(Utils::Format("N_btn-0%d", i).c_str()));
            ((u32*)currCupButton)[0x214/4] = i;

            if (isCupLockedArray[i]) {
                CourseManager::BaseMenuButtonControl_setTex((u32)currCupButton, 8, 2);
                CourseManager::BaseMenuButtonControl_setTex((u32)currCupButton, 1, 5);
                ((u32*)currCupButton)[0x230/4] = -1;
                ((u8*)currCupButton)[0x225/1] = 91;
            } else {
                CourseManager::BaseMenuButtonControl_setTex((u32)currCupButton, i, 2);
                ((u32*)currCupButton)[0x210/4] = i;
                ((u8*)currCupButton)[0x224/1] = 90;
                ((u32*)currCupButton)[0x230/4] = ((u8*)own)[0x2BC/1] ? 0 : -2;
                
            }

            own->GetButtonArray(0x2E0).Push(currCupButton);
        }

        VisualControl::GameVisualControl* cupCursor = VisualControl::Build(ownU32, "cup_cursor", "cup_cursor", (VisualControl::AnimationDefineVtable*)0x61EB40, (VisualControl::GameVisualControlVtable*)0x61EB64, VisualControl::ControlType::CUP_CURSOR_CONTROL);
        ownU32[0x2D8/4] = (u32)cupCursor;

        for (int i = 0; i < 8; i++) {
            baseMenuButtonControlSetCursor(own->GetButtonArray(0x2E0)[i], cupCursor, SafeStringBase("N_btn"));
        }

        if (!((u8*)own)[0x2BC/1]) {
            VisualControl::GameVisualControl* racestart = setupRaceDialogButton(own, SafeStringBase("race_startbtn"), SafeStringBase("race_startbtn"), 0);
            ((u32*)racestart)[0x210/4] = 9;
            ((u32*)racestart)[0x230/4] = 0;
            VisualControl::Message message;
            Language::MsbtHandler::GetMessageFromList(message, (Language::MsbtHandler::MessageDataList*)racestart->GetMessageDataList(), 2400);
            racestart->GetNwlytControl()->vtable->replaceMessageImpl(racestart->GetNwlytControl(), ((u32*)racestart)[0x94/4], message, nullptr, nullptr);
        }

        if (ownU32[0x284/4] & 1) {
            if (((u8*)own)[0x2BC/1]) {
                VisualControl::GameVisualControl* backButton = setupBackButton(own, SafeStringBase("cmn_back_btn"), SafeStringBase("cmn_back_btn"));
                ((u32*)backButton)[0x230/4] = ownU32[0x5C/4];
            } else {
                VisualControl::GameVisualControl* backButton = setupBackButtonBothControl(own, SafeStringBase("cmn_back_btn"), SafeStringBase("cmn_back_btn"));
                ((u32*)backButton)[0x210/4] = 8;
                ((u32*)backButton)[0x230/4] = ownU32[0x5C/4];
                ((u32*)backButton)[0x234/4] = -3;
            }
        }

        cursorMoveSetType(own->GetControlMove(), 2);
        own->GetControlInfo()[0xF4/4] = 4;
        ((u8*)own->GetControlInfo())[0xF9/1] = 0;
        ((u8*)own->GetControlInfo())[0x118/1] = 4;
    }

    void MenuPageHandler::MenuSingleCupGPPage::OnPageEnter(GameSequenceSection* own) {
        u32* ownU32 = (u32*)own;
        void(*moflexUpdateCup)(GameSequenceSection*) = (decltype(moflexUpdateCup))0x004A3E60;
        void(*MoflexEnablePane)(VisualControl::GameVisualControl*, bool enable) = (decltype(MoflexEnablePane))0x001843C0;
        void(*MoflexUpdateState)(bool enable) = (decltype(MoflexUpdateState))0x004A3D74;
        void(*courseButtonDmySelectOn)(VisualControl::GameVisualControl*) = (decltype(courseButtonDmySelectOn))0x0015C4B0;
        void(*courseButtonDmySelectOff)(VisualControl::GameVisualControl*) = (decltype(courseButtonDmySelectOff))0x0015C4D8;
        
        moflexUpdateCup(own);
        GarageRequestChangeState(MarioKartFramework::getGarageDirector(), 0xB, ownU32[0x48/4] == 1 ? 0 : 1);
        u32 lastSelectedCup = own->GetLastSelectedButton(); 
        if (lastSelectedCup != -1) {
            bool isCupLocked = true;
            if (lastSelectedCup < 8) {
                /* Check if cup is open
                bool isOpen = ((bool(*)(const int*))0x4D158C)(&i);
                */
               isCupLocked = false;
            }
            VisualControl::GameVisualControl* omakaseView = (VisualControl::GameVisualControl*)ownU32[0x2DC/4];
            u32 omakaseRootHandle = (u32)(omakaseView->vtable->getRootPane(omakaseView));
            omakaseView->GetNwlytControl()->vtable->setVisibleImpl(omakaseView->GetNwlytControl(), omakaseRootHandle, isCupLocked);
            VisualControl::GameVisualControl* movieView = (VisualControl::GameVisualControl*)ownU32[0x2C0/4]; 
            MoflexEnablePane(movieView, !isCupLocked);
            MoflexUpdateState(!isCupLocked);
            CourseManager::MenuSingle_CupBase_changeCup((u32)own, lastSelectedCup & 0xFF);
        }
        if (((u16*)own)[0x1E/2] != 1) {
            VisualControl::GameVisualControl** courseButtonDummies = (VisualControl::GameVisualControl**)(ownU32 + 0x2C8/4);
            for (int i = 0; i < 4; i++) {
                if (i == 0)
                    courseButtonDmySelectOn(courseButtonDummies[i]);
                else
                    courseButtonDmySelectOff(courseButtonDummies[i]);
            }
        }
    }

    void MenuPageHandler::MenuSingleCupGPPage::EnterCursor(GameSequenceSection* own, int cursor) {
        if (cursor >= 0)
            return;
        u32** sequeceEngine = (u32**)MarioKartFramework::getSequenceEngine();
        u32 cupID = sequeceEngine[0xE8/4][0x10/4];
        if (cupID < 8) {
            /* Check if cup is open
            bool isOpen = ((bool(*)(const int*))0x4D158C)(&i);
            */
            bool isOpen = true;
            if (!isOpen) sequeceEngine[0xE8/4][0x10/4] = 0;
        }
    }

    void MenuPageHandler::MenuSingleCupGPPage::OnSliderSetting(GameSequenceSection* own, bool flag1, bool flag2) {
        void(*BaseMenuPageOnSliderSettingImpl)(GameSequenceSection*,bool,bool,int) = (decltype(BaseMenuPageOnSliderSettingImpl))0x00472F1C;
        u32* ownU32 = (u32*)own;
        u8* ownU8 = (u8*)own;
        if (!flag1) {
            u8* backbuttonflag = ownU8 + 0x284/1;
            if (!flag2)
                *backbuttonflag |= 8;
            else
                *backbuttonflag &= ~8;
        }
        BaseMenuPageOnSliderSettingImpl(own, flag1, flag2, 3);
        if ((flag1 && !flag2) || (!flag1 && flag2)) {
            SeadArrayPtr<void*>& controlSliderArray = *(SeadArrayPtr<void*>*)(ownU32 + 0x26C/4);
            ((u8*)controlSliderArray[0])[0x1838/1] = 0;
            ((u8*)controlSliderArray[2])[0x1838/1] = 0;
        }
    }

    void MenuPageHandler::MenuSingleCupGPPage::OnPagePreStep(GameSequenceSection* own) {
        u32(*getSelectedCupCourse)(u32 moflexHandle) = (decltype(getSelectedCupCourse))0x004EA260;
        void(*courseButtonDmySelectOn)(VisualControl::GameVisualControl*) = (decltype(courseButtonDmySelectOn))0x0015C4B0;
        void(*courseButtonDmySelectOff)(VisualControl::GameVisualControl*) = (decltype(courseButtonDmySelectOff))0x0015C4D8;
        void(*controlSliderStepH)(void* controlSlider) = (decltype(controlSliderStepH))0x00481040;
        void(*controlSliderStepV)(void* controlSlider) = (decltype(controlSliderStepV))0x00481474;
        void(*basePageCalcNormalControl)(GameSequenceSection*) = (decltype(basePageCalcNormalControl))0x004D3B88;
        u32* ownU32 = (u32*)own;
        u8* ownU8 = (u8*)own;
        if (ownU8[0x14/1]) {
            u32 selectedCupCourse = getSelectedCupCourse(ownU32[0x294/4]);
            u32 selectedButtonID = own->GetLastSelectedButton();
            if (selectedCupCourse > 0 && selectedButtonID == (selectedCupCourse - 1) / 8 ) {
                u32 viewIndex = selectedCupCourse % 8;
                u32 selectedTrack;
                if (viewIndex == 1 || viewIndex == 2) {
                    selectedTrack = 0;
                } else if (viewIndex == 3 || viewIndex == 4) {
                    selectedTrack = 1;
                } else if (viewIndex == 5 || viewIndex == 6) {
                    selectedTrack = 2;
                } else if (viewIndex == 7 || viewIndex == 0) {
                    selectedTrack = 3;
                } else {
                    selectedTrack = -1;
                }
                VisualControl::GameVisualControl** courseButtonDummies = (VisualControl::GameVisualControl**)(ownU32 + 0x2C8/4);
                for (int i = 0; i < 4; i++) {
                    if (i == selectedTrack)
                        courseButtonDmySelectOn(courseButtonDummies[i]);
                    else
                        courseButtonDmySelectOff(courseButtonDummies[i]);
                }
            }
        }
        if (ownU8[0x318/1]) {
            u8 status = ownU8[0x8F/1];
            if (status == 2 || status == 5)
                ownU32[0x98/4] = 0;
            if (ownU32[0x314/4]) {
                ownU32[0x314/4]--;
            } else {
                SeadArrayPtr<void*>& controlSliderArray = *(SeadArrayPtr<void*>*)(ownU32 + 0x26C/4);
                void* defaultSlider = controlSliderArray[0];
                controlSliderStepH(defaultSlider);
                controlSliderStepV(defaultSlider);
                ((u32*)defaultSlider)[0]++;
                if (((u32*)defaultSlider)[0xC/4] && ((u8*)defaultSlider)[0x1866/1]) {
                    ownU8[0x318/1] = 0;
                }
            }
        }
        if (ownU8[0x8F/1] != 0 && ownU8[0x8F/1] != 1) {
            basePageCalcNormalControl(own);
        }
    }

    void MenuPageHandler::MenuSingleCupGPPage::ButtonHandler_SelectOn(GameSequenceSection* own, int buttonID) {
        u32* ownU32 = (u32*)own;
        void(*MoflexUpdateState)(bool enable) = (decltype(MoflexUpdateState))0x004A3D74;
        void(*MoflexEnablePane)(VisualControl::GameVisualControl*, bool enable) = (decltype(MoflexEnablePane))0x001843C0;
        void(*MoflexInit)(const SafeStringBase&,int) = (decltype(MoflexInit))0x004A43F0;
        void(*MoflexUpdateFrame)(u32, int) = (decltype(MoflexUpdateFrame))0x0017DC74;
        void(*courseButtonDmySelectOn)(VisualControl::GameVisualControl*) = (decltype(courseButtonDmySelectOn))0x0015C4B0;
        void(*courseButtonDmySelectOff)(VisualControl::GameVisualControl*) = (decltype(courseButtonDmySelectOff))0x0015C4D8;
        

        bool isCupLocked = true;
        if (buttonID < 8) {
             /* Check if cup is open
                bool isOpen = ((bool(*)(const int*))0x4D158C)(&i);
             */
            isCupLocked = false;
            if (isCupLocked) {
                MoflexUpdateState(false);
            }
        }
        VisualControl::GameVisualControl* omakaseView = (VisualControl::GameVisualControl*)ownU32[0x2DC/4];
        u32 omakaseRootHandle = (u32)(omakaseView->vtable->getRootPane(omakaseView));
        omakaseView->GetNwlytControl()->vtable->setVisibleImpl(omakaseView->GetNwlytControl(), omakaseRootHandle, isCupLocked);
        VisualControl::GameVisualControl* movieView = (VisualControl::GameVisualControl*)ownU32[0x2C0/4]; 
        MoflexEnablePane(movieView, !isCupLocked);
        VisualControl::GameVisualControl** courseButtonDummies = (VisualControl::GameVisualControl**)(ownU32 + 0x2C8/4);
        for (int i = 0; i < 4; i++) {
            if (i == 0)
                courseButtonDmySelectOn(courseButtonDummies[i]);
            else
                courseButtonDmySelectOff(courseButtonDummies[i]);
        }
        CourseManager::MenuSingle_CupBase_changeCup((u32)own, buttonID & 0xFF);
        if (buttonID < 8) {
            bool* isCupLockedArray = (bool*)(ownU32 + 0x30C/4);
            if (!isCupLockedArray[buttonID]) {
                if (!((u32*)(ownU32[0x294/4]))[0x16B8/4]) {
                    MoflexInit(SafeStringBase("CourseSelectRace"), 0);
                }
                u32* someArray = ownU32 + 0x29C/4;
                u32 index = someArray[buttonID];
                if (index == 0)
                    index = someArray[0];
                MoflexUpdateFrame(ownU32[0x294/4], index);
            }
        }
    }

    void MenuPageHandler::MenuSingleCupGPPage::ButtonHandler_OK(GameSequenceSection* own, int buttonID) {
        u32* ownU32 = (u32*)own;

        void(*controlSliderStartH)(void* controlSlider) = (decltype(controlSliderStartH))0x004815FC;
        void(*controlSliderStartV)(void* controlSlider) = (decltype(controlSliderStartV))0x00481868;
        void(*garageDirectorFade)(u32 garageDirector, bool fade) = (decltype(garageDirectorFade))0x003F9898;
        
        if (buttonID == 9)
            return;
        
        if (buttonID != 8) {
            MarioKartFramework::BasePageSetCup(buttonID & 0xFF);
            if (!((u8*)own)[0x2BC/1]) {
                GarageRequestChangeState(MarioKartFramework::getGarageDirector(), 0xF, 1);
            }
            ownU32[0x318/4] = 1;
            ownU32[0x314/4] = 0;
            SeadArrayPtr<void*>& controlSliderArray = *(SeadArrayPtr<void*>*)(ownU32 + 0x26C/4);
            ((u8*)controlSliderArray[0])[0x1838] = 1;
            ((u8*)controlSliderArray[1])[0x1838] = 0;
            ((u8*)controlSliderArray[2])[0x1838] = 0;
            ((u8*)controlSliderArray[0])[0x1858] = 0;
            ((u8*)controlSliderArray[0])[0x4] = 0;
            ((u32*)controlSliderArray[0])[0] = 0;
            controlSliderStartH(controlSliderArray[0]);
            controlSliderStartH(controlSliderArray[0]);
        } else {
            if (((u8*)own)[0x8F/1]) {
                ownU32[0x318/4] = 1;
                ownU32[0x314/4] = 0;
                SeadArrayPtr<void*>& controlSliderArray = *(SeadArrayPtr<void*>*)(ownU32 + 0x26C/4);
                ((u8*)controlSliderArray[0])[0x1838] = 2;
                ((u8*)controlSliderArray[1])[0x1838] = 0;
                ((u8*)controlSliderArray[2])[0x1838] = 0;
                ((u8*)controlSliderArray[0])[0x1858] = 1;
                ((u8*)controlSliderArray[0])[0x4] = 1;
                ((u32*)controlSliderArray[0])[0] = 0;
                controlSliderStartH(controlSliderArray[0]);
                controlSliderStartH(controlSliderArray[0]);
            }
            u32 garageDirector = MarioKartFramework::getGarageDirector();
            garageDirectorFade(garageDirector, true);
            GarageRequestChangeState(garageDirector, 0xB, 0);
        }
    }

    void MenuPageHandler::MenuSingleCupGPPage::OnPageComplete(GameSequenceSection* own) {
        void(*SequenceStartFadeout)(int faderType, u32 frames, int faderScreen) = (decltype(SequenceStartFadeout))0x00480430;
        void(*SequenceReserveFadeIn)(int faderType, u32 frames, int faderScreen) = (decltype(SequenceStartFadeout))0x004841FC;
        
        u32* ownU32 = (u32*)own;
        if (ownU32[0x4C/4] != ownU32[0x5C/4]) {
            ((void(*)())(UserCTHandler::BaseMenuPage_applySetting_GP))();
            SequenceStartFadeout(1, 30, 2);
            SequenceReserveFadeIn(1, 30, 2);
            GarageRequestChangeState(MarioKartFramework::getGarageDirector(), 0x10, 1);
        }
    }

    void MenuPageHandler::MenuSingleCupGPPage::OnPageExit(GameSequenceSection* own) {
        u32* ownU32 = (u32*)own;
        void(*MoflexUpdateState)(bool enable) = (decltype(MoflexUpdateState))0x004A3D74;
        float(*AnimationItemGetCurrentFrame)(u32*) = (decltype(AnimationItemGetCurrentFrame))0x0015B004;

        VisualControl::GameVisualControl* movieView = (VisualControl::GameVisualControl*)ownU32[0x2C0/4];
        bool somethingequal = ownU32[0x4C/4] == ownU32[0x5C/4];
        if (somethingequal) {
            MoflexUpdateState(true);
        }
        ((u32*)movieView)[0xAC/4] = somethingequal;
        VisualControl::GameVisualControl* omakaseView = (VisualControl::GameVisualControl*)ownU32[0x2DC/4];
        VisualControl::AnimationFamily* omakaseAnimFamily = omakaseView->GetAnimationFamily(0);
        struct
        {
            u32 size;
            u32** data;
            u32 count;
        } *animationItemArray = (decltype(animationItemArray)) ((u32)omakaseAnimFamily + 0xC);
        u32* animationItem = animationItemArray->data[animationItemArray->count < animationItemArray->size ? animationItemArray->count : 0];
        *(float*)0x65E988 = AnimationItemGetCurrentFrame(animationItem);
    }
    
    // Replaces the constructSection function
    void* MenuPageHandler::LoadSingleCupGPMenu(void* sectionDirector) {
        if (!menuSingleCupGP) {
            menuSingleCupGP = new MenuSingleCupGPPage();
            return (void*)menuSingleCupGP->Load(sectionDirector);
        }
        return nullptr;
    }

    void MenuPageHandler::OnMenuSingleCupGPDeallocate(GameSequenceSection* own) {
        if (!menuSingleCupGP) return;
        MenuSingleCupGPPage* page = (MenuSingleCupGPPage*)own->vtable->userData;
        page->Deallocate();
        delete menuSingleCupGP;
        menuSingleCupGP = nullptr;
    }